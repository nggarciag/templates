# Politica de seguridad

Para nuevos anuncios de seguridad remitirse a

## Versiones soportadas
Constantemente estamos sacando versiones de seguridad.

| CVSS v3.0   | Supported Versions |
| ----------- | ----------- |
| 9.0-10.0    | Releases within the previous three months      |
| 4.0-8.9   | Most recent release        |

## Reportando una vulnerabilidad

Please report (suspected) security vulnerabilities to security@ory.sh. You will receive a response from us within 48 hours. If the issue is confirmed, we will release a patch as soon as possible depending on complexity but historically within a few days.