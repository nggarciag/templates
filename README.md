# Título del Proyecto

_Escribir una descripcion de lo que es el proyecto_

## Tabla de contenido
1. [Estado del proyecto](#estado-del-proyecto)
2. [Empezando](#empezando)
3. [Pre-requisitos](#pre-requisitos)
4. [Instalación](#instalación)
5. [Pruebas](#pruebas)
6. [Despliegue](#despliegue)
7. [Construido con](#construido-con)
8. [Soporte e issues](#soporte-e-issues)
9. [Bugs](#bugs)
10. [FAQs](#faqs)
11. [Versionado](#versionado)
12. [Autores](#autores)
13. [Licencia](#licencia)


## Estado del proyecto

_Escribir una descripcion sobre el estado del proyecto, se debe mencionar si el proyecto esta en desarrollo o terminado_

## Empezando 

_Escribir las instrucciones permitirán obtener una clonacion del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 

_Escribir los Pre-requisitos para para instalar el software e instrucciones sobre como instalarlas_

```
Dar un ejemplo
```

### Instalación 

_Escribir una serie de ejemplos con los paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Dí cómo será ese paso_

```
Da un ejemplo
```

_Y repite_

```
hasta finalizar
```

_Finaliza con un ejemplo de cómo obtener datos del sistema o como usarlos para una pequeña demo_

## Pruebas

_Escribir y explicar como ejecutar las pruebas automatizadas para este sistema y por que_

_Escribir los tipos de pruebas que incorpora, ejmp: Blackbox Test, Perfomance Tests, Pruebas Funcionales, Pruebas Unitarias, etc_

```
Da un ejemplo
```

## Despliegue 

_Escribir los pasos adicionales sobre como hacer deploy del proyecto_

## Construido con 

_Escribir las herramientas que se utilizaron para crear el proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Soporte e issues

Por favor ir a (https://gist.github.com/villanuevand/xxxxxx) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Bugs

_Escribir una lista de los bugs conocidos y posibles correciones de errores_

## FAQs

_Seccion de preguntas frecuentes con todas las preguntas planteadas hasta la fecha_

## Versionado 

Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores 

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Andrés Villanueva** - *Trabajo Inicial* - [villanuevand](https://github.com/villanuevand)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto. 

## Licencia 

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles



